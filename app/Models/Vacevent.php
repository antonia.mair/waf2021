<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Vacevent extends Model
{
    use HasFactory;

    protected $fillable = ['id', 'location_id', 'vacdate', 'maxPers'];


    public function location():BelongsTo{
        return $this->belongsTo(Location::class);
    }

    public function vaccinateds():HasMany{
        return $this->hasMany(Vaccinated::class);
    }
}
