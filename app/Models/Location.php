<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Location extends Model
{
    use HasFactory;

    protected $fillable = ['id', 'bezeichnung', 'street', 'plz', 'ort', 'capacity', 'district_id'];

    public function vacevents(): HasMany
    {
        return $this->hasMany(Vacevent::class);
    }

    public function district(): BelongsTo{
        return $this->belongsTo(District::class);
    }
}
