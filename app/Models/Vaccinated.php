<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Vaccinated extends Model
{
    use HasFactory;

    protected $fillable = ['id', 'firstname', 'lastname', 'sex', 'birthdate', 'svnr', 'email',
        'tel', 'vaccinated', 'vacevent_id', 'user_id'];

    public function vacevent():BelongsTo{
        return $this->belongsTo(Vacevent::class);
    }

    public function user():BelongsTo{
        return $this->belongsTo(User::class);
    }
}
