<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'password',];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return ['user' => ['id' => $this->id, 'isAdmin' => $this->isAdmin(), 'vaccinated_id' => $this->getVacId()]];
    }

    public function roles():BelongsToMany{
        return $this->belongsToMany(Role::class)->withTimestamps();
    }

    public function vaccinated():BelongsTo{
        return $this->belongsTo(Vaccinated::class);
    }

    public function getVacId(){
        $vaccinated = Vaccinated::where('user_id', $this->id)->first();
        if($vaccinated)
            return $vaccinated->id;
        else return 0;
    }

    public function isAdmin(){
        $roles = $this->roles()->get();
        if($roles){
            foreach($roles as $role){
                if ($role->id == 1)return 1;
            }
            return 0;
        }
        else return 0;
    }
}
