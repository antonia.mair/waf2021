<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Models\Vacevent;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LocationController extends Controller
{
    //

    public function index(){
        $locations = Location::with(['vacevents', 'district'])->get();
        return $locations;
    }

    public function getById($id){
        $location = Location::where('id', $id)->with(['vacevents', 'district'])->first();
        return $location;
    }

    public function getByDistrictId($district_id){
        $locations = Location::where('district_id', $district_id)->with(['vacevents'])->first();
        return $locations;
    }

    public function delete($id) : JsonResponse
    {
        $location = Location::where('id', $id)->first();
        if ($location != null) {
            $location->delete();
        }
        else
            throw new \Exception("This location couldn't be deleted because it does not exist");
        return response()->json('Location with ID ' . $id . ' successfully deleted', 200);
    }

    public function update(Request $request, int $id):JsonResponse {
        DB::beginTransaction();
        try {
            $location = Location::with(['vacevents', 'district'])
                ->where('id', $id)->first();
            if ($location != null) {
                $location->update($request->all());

                //update vacevents
                $ids = [];
                if (isset($request['vacevents']) && is_array($request['vacevents'])) {
                    foreach ($request['vacevents'] as $vacevent) {
                        array_push($ids,$vacevent['id']);
                    }
                }
                $location->vacevents()->saveMany($ids);
                $location->save();
            }
            DB::commit();
            $location1 = Location::with(['vacevents', 'district'])
                ->where('id', $id)->first();
            // return a valid http response
            return response()->json($location1, 201);
        }
        catch (\Exception $e) {
            // rollback all queries
            DB::rollBack();
            return response()->json("Updating Location failed: " . $e->getMessage(), 420);
        }
    }

    public function save(Request $request) : JsonResponse  {
        DB::beginTransaction();
        try {
            $location = Location::create($request->all());
            DB::commit();
            // return a vaild http response
            return response()->json($location, 201);
        }
        catch (\Exception $e) {
            // rollback all queries
            DB::rollBack();
            return response()->json("Saving Location failed: " . $e->getMessage(), 420);
        }
    }
}
