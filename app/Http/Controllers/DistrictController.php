<?php

namespace App\Http\Controllers;

use App\Models\District;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DistrictController extends Controller
{
    //
    public function index(){
        $districts = District::with(['locations', 'state'])->get();
        return $districts;
    }

    public function getById($id){
        $district = District::where('id', $id)->with(['locations', 'state'])->first();
        return $district;
    }

    public function getbyStateId($state_id){
        $districts = District::where('state_id', $state_id)->with(['locations'])->get();
        return $districts;
    }

    /*public function delete($id) : JsonResponse
    {
        $district = District::where('id', $id)->first();
        if ($district != null) {
            $district->delete();
        }
        else
            throw new \Exception("This district couldn't be deleted because it does not exist");
        return response()->json('District with ID ' . $id . ' successfully deleted', 200);
    }*/
}
