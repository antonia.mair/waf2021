<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Models\Vaccinated;
use App\Models\Vacevent;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VaceventController extends Controller
{
    //
    public function index(){
        $vacevents = Vacevent::with(['location', 'vaccinateds'])->get();
        return $vacevents;
    }

    public function getById($id){
        $vacevent = Vacevent::where('id', $id)->with(['location', 'vaccinateds'])->first();
        return $vacevent;
    }

    public function getbyVaccinatedId($vaccinated_id){
        $vacevent = Vaccinated::where('id', $vaccinated_id)->first()
            ->vacevent()
            ->with(['location'])
            ->first();
        return $vacevent;
    }

    public function getDeletable($id){

        $vaccinated = Vaccinated::where('vacevent_id', $id)->first();
        return $vaccinated == null ? response()->json(true, 200): response()->json(false, 200);
    }

    public function isFull($id){
        $registeredPers = Vaccinated::where('vacevent_id', $id)->count();
        $maxPers = Vacevent::where('id', $id)->first()['maxPers'];
        //dd($maxPers, $registeredPers);
        return ($maxPers <= $registeredPers) ? response()->json(true, 200): response()->json(false, 200);
    }


    public function getbyLocationId($location_id){
        $unfvacevents = Vacevent::where('location_id', $location_id)->get();
        $vacevents = [];
        foreach ($unfvacevents as $vacevent){
            $registeredPers = Vaccinated::where('vacevent_id', $vacevent['id'])->count();

            if(/*$vacevent['maxPers'] > $registeredPers &&*/ $vacevent['vacdate'] >= date('Y-m-d H:i:s')){
                array_push($vacevents, $vacevent);
            }
        }
        return $vacevents;
    }

    public function delete($id) : JsonResponse
    {
        $vacevent = Vacevent::where('id', $id)->first();
        if ($vacevent != null) {
            $vacevent->delete();
        }
        else
            throw new \Exception("This Event couldn't be deleted because it does not exist");
        return response()->json('Event with ID ' . $id . ' successfully deleted', 200);
    }

    public function update(Request $request, int $id):JsonResponse {
        DB::beginTransaction();
        try {
            $vacevent = Vacevent::with(['location', 'vaccinateds'])
                ->where('id', $id)->first();
            //$registeredPers = Vaccinated::where('vacevent_id', $vacevent['id'])->count();
            if ($vacevent != null /*&& $registeredPers < 1*/) {
                $request = $this->parseRequest($request);
                $vacevent->update($request->all());

                /*$maxPers = $vacevent['maxPers'];
                $registeredPers = Vaccinated::where('vacevent_id', $vacevent['id']);*/

                DB::commit();
                $vacevent1 = Vacevent::with(['location', 'vaccinateds'])
                    ->where('id', $id)->first();
                // return a valid http response
                return response()->json($vacevent1, 201);

            }
            DB::rollBack();
            return response()->json("Updating Vacevent failed: no updateable vacevent", 420);
        }
        catch (\Exception $e) {
            // rollback all queries
            DB::rollBack();
            return response()->json("Updating Vacevent failed: " . $e->getMessage(), 420);
        }
    }

    public function parseRequest(Request $request){
        $date = new \DateTime($request->vacdate);
        $request['vacdate'] = $date;
        return $request;
    }

    public function save(Request $request) : JsonResponse  {
        DB::beginTransaction();
        try {
            $request = $this->parseRequest($request);
            $vacevent = Vacevent::create($request->all());
            DB::commit();
            // return a vaild http response
            return response()->json($vacevent, 201);
        }
        catch (\Exception $e) {
            // rollback all queries
            DB::rollBack();
            return response()->json("Saving Vacevent failed: " . $e->getMessage(), 420);
        }
    }



}
