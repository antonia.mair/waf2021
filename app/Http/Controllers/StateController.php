<?php

namespace App\Http\Controllers;

use App\Models\State;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class StateController extends Controller
{
    //
    public function index(){
        $states = State::with(['districts'])->get();
        return $states;
    }

    public function getById($id){
        $state = State::where('id', $id)->with(['districts'])->first();
        return $state;
    }

    /*public function delete($id) : JsonResponse
    {
        $state = State::where('id', $id)->first();
        if ($state != null) {
            $state->delete();
        }
        else
            throw new \Exception("This state couldn't be deleted because it does not exist");
        return response()->json('State with ID ' . $id . ' successfully deleted', 200);
    }*/
}
