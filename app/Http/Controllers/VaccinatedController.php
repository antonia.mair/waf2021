<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Role;
use App\Models\User;
use App\Models\Vacevent;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\Vaccinated;
use Illuminate\Support\Facades\DB;
use function PHPUnit\Framework\throwException;

class VaccinatedController extends Controller
{

    public function index(){
        $vaccinateds = Vaccinated::with(['vacevent'])->get();
        return $vaccinateds;
    }

    public function getById($id){
        $vaccinated = Vaccinated::where('id', $id)->with(['vacevent'])->first();
        return $vaccinated;
    }

    public function getbyVaceventId($vacevent_id){
        $vaccinateds = Vaccinated::with(['vacevent'])->where('vacevent_id', $vacevent_id)->get();
        return $vaccinateds;
    }

    public function getbyUserId($user_id){
        $vaccinated_id = Vaccinated::where('user_id', $user_id)->first()->id;
        return $vaccinated_id;
    }

    public function checkSvnr($svnr){
        $vaccinateds = Vaccinated::where('svnr', $svnr)->first();
        return $vaccinateds != null ? response()->json(true, 200): response()->json(false, 200);
    }


    public function delete($id) : JsonResponse
    {
        $vaccinated = Vaccinated::where('id', $id)->first();
        if ($vaccinated != null) {
            $user = User::where('id', $vaccinated->user_id)->first();
            $vaccinated->delete();
            $user->delete();
        }
        else
            throw new \Exception("This Person couldn't be deleted because it does not exist");
        return response()->json('Person with ID ' . $id . ' successfully deleted', 200);
    }

    public function update(Request $request, int $id):JsonResponse {
        DB::beginTransaction();
        try {
            $vaccinated = Vaccinated::with(['vacevent', 'user'])
                ->where('id', $id)->first();
            $request = $this->parseRequest($request);
            if ($vaccinated != null) {
                if(isset($request['vacevent_id'])){
                    if($vaccinated['vaccinated'] == 0){
                        $vaccinated->update($request->all());
                        /*if(isset($request['email'])){
                            User::where('id', $vaccinated->user_id)->update(["email" => $request['email']]);
                        }*/
                        DB::commit();
                        $vaccinated1 = Vaccinated::with(['vacevent'])
                            ->where('id', $id)->first();
                        // return a valid http response
                        return response()->json($vaccinated1, 201);
                    }
                    else{
                        DB::rollBack();
                        return response()->json("Updating Vaccinated Person failed: could not update because the person is already vaccinated.", 420);
                    }
                }
                else{
                    $vaccinated->update($request->all());
                }
                /*if(isset($request['email'])){
                    User::where('id', $vaccinated->user_id)->update(["email" => $request['email']]);
                }*/
            }
            DB::commit();
            $vaccinated1 = Vaccinated::with(['vacevent'])
                ->where('id', $id)->first();
            // return a valid http response
            return response()->json($vaccinated1, 201);
        }
        catch (\Exception $e) {
            // rollback all queries
            DB::rollBack();
            return response()->json("Updating Vaccinated Person failed: " . $e->getMessage(), 420);
        }
    }

    /*public function update(Request $request, int $id):JsonResponse {
        DB::beginTransaction();
        try {
            $vaccinated = Vaccinated::with(['vacevent', 'user'])
                ->where('id', $id)->first();

            $request = $this->parseRequest($request);
            if ($vaccinated != null) {
                if(isset($request['vacevent_id'])){
                    $maxPers = Vacevent::where('id', $request['vacevent_id'])->value('maxPers');
                    $registeredPers = Vaccinated::where('vacevent_id', $request['vacevent_id'])->count();
                    if($maxPers > $registeredPers){
                        if($vaccinated['vaccinated'] == 0){
                            $vaccinated->update($request->all());
                            if(isset($request['email'])){
                                User::where('id', $vaccinated->user_id)->update(["email" => $request['email']]);
                            }
                            DB::commit();
                            $vaccinated1 = Vaccinated::with(['vacevent'])
                                ->where('id', $id)->first();
                            // return a valid http response
                            return response()->json($vaccinated1, 201);
                        }
                        else{
                            DB::rollBack();
                            return response()->json("Updating Vaccinated Person failed: could not update because the person is already vaccinated.", 420);
                        }
                    }
                    else{
                        DB::rollBack();
                        return response()->json("Updating Vaccinated Person failed: could not update because the event is already fully booked.", 420);
                    }
                }
                else{
                    $vaccinated->update($request->all());
                }

                if(isset($request['email'])){
                    User::where('id', $vaccinated->user_id)->update(["email" => $request['email']]);
                }
            }
            DB::commit();
            $vaccinated1 = Vaccinated::with(['vacevent'])
                ->where('id', $id)->first();
            // return a valid http response
            return response()->json($vaccinated1, 201);
        }
        catch (\Exception $e) {
            // rollback all queries
            DB::rollBack();
            return response()->json("Updating Vaccinated Person failed: " . $e->getMessage(), 420);
        }
    }*/

    public function save(Request $request) : JsonResponse  {
        DB::beginTransaction();
        try {
            $request = $this->parseRequest($request);
            $vaccinated = Vaccinated::create($request->all());

            /*$user =   User::firstOrNew(['email'=>$vaccinated['email'], 'vaccinated_id' =>$vaccinated['id']]);
            $user->save();
            $user->roles()->attach(Role::where('bezeichnung', 'User')->first());*/
            DB::commit();
            // return a vaild http response

            return response()->json($vaccinated, 201);
        }
        catch (\Exception $e) {
            // rollback all queries
            DB::rollBack();
            return response()->json("Saving Vaccinated Person failed: " . $e->getMessage(), 420);
        }
    }


    /*public function save(Request $request) : JsonResponse  {
        DB::beginTransaction();
        try {
            $request = $this->parseRequest($request);
            $vaccinated = Vaccinated::create($request->all());

            $user =   User::firstOrNew(['email'=>$vaccinated['email'], 'vaccinated_id' =>$vaccinated['id']]);
            $user->save();
            $user->roles()->attach(Role::where('bezeichnung', 'User')->first());
            DB::commit();
            // return a vaild http response

            return response()->json($vaccinated, 201);
        }
        catch (\Exception $e) {
            // rollback all queries
            DB::rollBack();
            return response()->json("Saving Vaccinated Person failed: " . $e->getMessage(), 420);
        }
    }*/

    public function parseRequest(Request $request){
        $date = new \DateTime($request->birthdate);
        $request['birthdate'] = $date;
        return $request;
    }


}
