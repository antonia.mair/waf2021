<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DistrictController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\StateController;
use App\Http\Controllers\VaccinatedController;
use App\Http\Controllers\VaceventController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('locations', [LocationController::class, 'index']);
Route::get('vaccinateds', [VaccinatedController::class, 'index']);
Route::get('vacevents', [VaceventController::class, 'index']);
Route::get('states', [StateController::class, 'index']);
Route::get('districts', [DistrictController::class, 'index']);

Route::get('location/{id}', [LocationController::class, 'getbyId']);
Route::get('vaccinated/{id}', [VaccinatedController::class, 'getbyId']);
Route::get('vacevent/{id}', [VaceventController::class, 'getbyId']);
Route::get('state/{id}', [StateController::class, 'getbyId']);
Route::get('district/{id}', [DistrictController::class, 'getbyId']);


Route::get('district/search/{state_id}', [DistrictController::class, 'getbyStateId']);
Route::get('location/search/{district_id}', [LocationController::class, 'getbyDistrictId']);
Route::get('vacevent/search/{vaccinated_id}', [VaceventController::class, 'getbyVaccinatedId']);
Route::get('vacevent/searchloc/{location_id}', [VaceventController::class, 'getbyLocationId']);
Route::get('vaccinated/search/{vacevent_id}', [VaccinatedController::class, 'getbyVaceventId']);
Route::get('vacevent/deletable/{id}', [VaceventController::class, 'getDeletable']);
Route::get('vacevent/isFull/{id}', [VaceventController::class, 'isFull']);
Route::get('vaccinated/user/{user_id}', [VaccinatedController::class, 'getbyUserId']);
Route::get('vaccinated/checksvnr/{svnr}', [VaccinatedController::class, 'checkSvnr']);


Route::post('auth/login', [AuthController::class,'login']);

Route::group(['middleware' => ['api','auth.jwt']], function(){
    Route::delete('location/{id}', [LocationController::class, 'delete']);
    Route::delete('vaccinated/{id}', [VaccinatedController::class, 'delete']);
    Route::delete('vacevent/{id}', [VaceventController::class, 'delete']);
    Route::delete('state/{id}', [StateController::class, 'delete']);
    Route::delete('district/{id}', [DistrictController::class, 'delete']);

    Route::put('location/{id}', [LocationController::class, 'update']);
    Route::put('vaccinated/{id}', [VaccinatedController::class, 'update']);
    Route::put('vacevent/{id}', [VaceventController::class, 'update']);

    Route::post('location', [LocationController::class, 'save']);
    Route::post('vaccinated', [VaccinatedController::class, 'save']);
    Route::post('vacevent', [VaceventController::class, 'save']);
    Route::post('auth/logout', [AuthController::class,'logout']);
});






