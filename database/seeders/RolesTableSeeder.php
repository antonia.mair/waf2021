<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role = new Role;
        $role->bezeichnung = 'Admin';
        $role->save();



        $role2 = new Role;
        $role2->bezeichnung = 'User';
        $role2->save();



    }
}
