<?php

namespace Database\Seeders;

use App\Models\Location;
use App\Models\Vaccinated;
use App\Models\Vacevent;
use DateTime;
use Illuminate\Database\Seeder;

class VaceventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $vacevent = new Vacevent;
        $vacevent->vacdate = new DateTime();
        $location = Location::all()->first();
        $vacevent->location()->associate($location);
        $vacevent->maxPers = Location::all()->first()->capacity;
        $vacevent->save();

    }
}
