<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use App\Models\Vaccinated;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = new User;
        //$user->name = 'admin';
        $user->email = 'admint@test.com';
        $user->password = bcrypt('123456');
        $user->save();

        $user1 = new User;
        $user1->email = 'vacpers1@test.com';
        $user1->password = bcrypt('123456');
        $user1->save();

        $user2 = new User;
        $user2->email = 'vacpers2@test.com';
        $user2->password = bcrypt('123456');
        $user2->save();

        $user3 = new User;
        $user3->email = 'vacpers3@test.com';
        $user3->password = bcrypt('123456');
        $user3->save();

        $role = new Role;
        $role->bezeichnung = 'Admin';
        $role->save();
        $role->users()->attach($user);
        $role->save();

        $role2 = new Role;
        $role2->bezeichnung = 'User';
        $role2->save();
        $users = User::all()->pluck('id');
        $role2->users()->sync($users);
        $role->save();



    }
}
