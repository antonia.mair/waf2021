<?php

namespace Database\Seeders;

use App\Models\District;
use App\Models\State;
use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $state = new State;
        $state->bezeichnung = 'Oberösterreich';
        $state->save();

            $district = new District;
            $district->bezeichnung = 'Linz';


            $district2 = new District;
            $district2->bezeichnung = 'Freistadt';


            $district3 = new District;
            $district3->bezeichnung = 'Linz-Land';


            $district4 = new District;
            $district4->bezeichnung = 'Wels-Land';


            $district5 = new District;
            $district5->bezeichnung = 'Vöcklabruck';


            $district6 = new District;
            $district6->bezeichnung = 'Urfahr-Umgebung';


            $district7 = new District;
            $district7->bezeichnung = 'Steyr-Land';


            $district8 = new District;
            $district8->bezeichnung = 'Schärding';

            $district9 = new District;
            $district9->bezeichnung = 'Rohrbach';


            $district10 = new District;
            $district10->bezeichnung = 'Ried';


            $district11 = new District;
            $district11->bezeichnung = 'Perg';


            $district12 = new District;
            $district12->bezeichnung = 'Kirchdorf';


            $district13 = new District;
            $district13->bezeichnung = 'Grieskirchen';


            $district14 = new District;
            $district14->bezeichnung = 'Gmunden';


            $district15 = new District;
            $district15->bezeichnung = 'Eferding';


            $district16 = new District;
            $district16->bezeichnung = 'Braunau';


            $state->districts()->saveMany([$district, $district2, $district3, $district4, $district5, $district6,
                $district7, $district8, $district9, $district10, $district11, $district12, $district13,
                $district14, $district15, $district16]);
            $state->save();

        $state2 = new State;
        $state2->bezeichnung = 'Salzburg';
        $state2->save();

            $district17 = new District;
            $district17->bezeichnung = 'Hallein';


            $district18 = new District;
            $district18->bezeichnung = 'Salzburg Umgebung';


            $district19 = new District;
            $district19->bezeichnung = 'St. Johann im Pongau';


            $district20 = new District;
            $district20->bezeichnung = 'Tamsweg';


            $district21 = new District;
            $district21->bezeichnung = 'Zell am See';

            $district22 = new District;
            $district22->bezeichnung = 'Salzburg Stadt';


            $state2->districts()->saveMany([ $district17, $district18,
                $district19, $district20, $district21, $district22]);
            $state2->save();


        $state4 = new State;
        $state4->bezeichnung = 'Niederösterreich';
        $state4->save();

            $district23 = new District;
            $district23->bezeichnung = 'Amstetten';


            $district24 = new District;
            $district24->bezeichnung = 'Baden';


            $district25 = new District;
            $district25->bezeichnung = 'Bruck a.d. Leitha';


            $district26 = new District;
            $district26->bezeichnung = 'Gänserndorf';


            $district27 = new District;
            $district27->bezeichnung = 'Gmünd';


            $district28 = new District;
            $district28->bezeichnung = 'Hollabrunn';


            $district29 = new District;
            $district29->bezeichnung = 'Horn';


            $district30 = new District;
            $district30->bezeichnung = 'Korneuburg';


            $district31 = new District;
            $district31->bezeichnung = 'Krems';


            $district32 = new District;
            $district32->bezeichnung = 'Lilienfeld';


            $district33 = new District;
            $district33->bezeichnung = 'Melk';


            $district34 = new District;
            $district34->bezeichnung = 'Mistelbach';


            $district35 = new District;
            $district35->bezeichnung = 'Mödling';


            $district36 = new District;
            $district36->bezeichnung = 'Neunkirchen';


            $district37 = new District;
            $district37->bezeichnung = 'Scheibbs';


            $district38 = new District;
            $district38->bezeichnung = 'St. Pölten';


            $district39 = new District;
            $district39->bezeichnung = 'Tulln';


            $district40 = new District;
            $district40->bezeichnung = 'Waidhofen a.d. Thaya';


            $district41 = new District;
            $district41->bezeichnung = 'Wiener Neustadt';


            $district42 = new District;
            $district42->bezeichnung = 'Zwettl';


            $state4->districts()->saveMany([$district23, $district24, $district25, $district26, $district27,
                $district28, $district29, $district30, $district31, $district32, $district33,
                $district34, $district35, $district36, $district37, $district38, $district39, $district40,
                $district41, $district42]);
            $state4->save();


        $state5 = new State;
        $state5->bezeichnung = 'Burgenland';
        $state5->save();

            $district43 = new District;
            $district43->bezeichnung = 'Eisenstadt Stadt';


            $district44 = new District;
            $district44->bezeichnung = 'Eisenstadt Umgebung';


            $district45 = new District;
            $district45->bezeichnung = 'Güssing';


            $district46 = new District;
            $district46->bezeichnung = 'Jennersdorf';


            $district47 = new District;
            $district47->bezeichnung = 'Mattersburg';


            $district48 = new District;
            $district48->bezeichnung = 'Neusiedl am See';


            $district49 = new District;
            $district49->bezeichnung = 'Oberpullendorf';

            $district50 = new District;
            $district50->bezeichnung = 'Oberwart';


            $district51 = new District;
            $district51->bezeichnung = 'Rust';


            $state5->districts()->saveMany([ $district43, $district44, $district45, $district46,
                $district47, $district48, $district49, $district50, $district51]);
            $state5->save();


        $state7 = new State;
        $state7->bezeichnung = 'Kärnten';
        $state7->save();

            $district52 = new District;
            $district52->bezeichnung = 'Feldkirchen';


            $district53 = new District;
            $district53->bezeichnung = 'Hermagor';


            $district54 = new District;
            $district54->bezeichnung = 'Klagenfurt Land';


            $district55 = new District;
            $district55->bezeichnung = 'Spittal a.d. Drau';


            $district56 = new District;
            $district56->bezeichnung = 'St. Veith a.d. Glan';


            $district57 = new District;
            $district57->bezeichnung = 'Villach Land';


            $district58 = new District;
            $district58->bezeichnung = 'Völkermarkt';


            $district59 = new District;
            $district59->bezeichnung = 'Wolfsberg';


            $district60 = new District;
            $district60->bezeichnung = 'Klagenfurt Stadt';


            $district61 = new District;
            $district61->bezeichnung = 'Villach Stadt';


            $state7->districts()->saveMany([$district52, $district53, $district54, $district55, $district56,
                $district57, $district58, $district59, $district60, $district61]);
            $state7->save();


        $state8 = new State;
        $state8->bezeichnung = 'Steiermark';
        $state8->save();

            $district62 = new District;
            $district62->bezeichnung = 'Graz';


            $district63 = new District;
            $district63->bezeichnung = 'Deutschlandsberg';


            $district64 = new District;
            $district64->bezeichnung = 'Graz-Umgebung';


            $district65 = new District;
            $district65->bezeichnung = 'Leibnitz';


            $district66 = new District;
            $district66->bezeichnung = 'Leoben';


            $district67 = new District;
            $district67->bezeichnung = 'Liezen';


            $district68 = new District;
            $district68->bezeichnung = 'Murau';


            $district69 = new District;
            $district69->bezeichnung = 'Voitsberg';


            $district70 = new District;
            $district70->bezeichnung = 'Weiz';


            $district71 = new District;
            $district71->bezeichnung = 'Murtal';


            $district72 = new District;
            $district72->bezeichnung = 'Bruck-Mürzzuschlag';


            $district73 = new District;
            $district73->bezeichnung = 'Hartberg-Fürstenfeld';


            $district74 = new District;
            $district74->bezeichnung = 'Südoststeiermark';


            $state8->districts()->saveMany([$district62, $district63, $district64, $district65, $district66,
                $district67, $district68, $district69, $district70, $district71, $district72,
                $district73, $district74]);
            $state8->save();

        $state9 = new State;
        $state9->bezeichnung = 'Tirol';
        $state9->save();

            $district75 = new District;
            $district75->bezeichnung = 'Imst';


            $district76 = new District;
            $district76->bezeichnung = 'Innsbruck-Land';


            $district77 = new District;
            $district77->bezeichnung = 'Innsbruck-Stadt';


            $district78 = new District;
            $district78->bezeichnung = 'Kitzbühel';


            $district79 = new District;
            $district79->bezeichnung = 'Kufstein';


            $district80 = new District;
            $district80->bezeichnung = 'Landeck';


            $district81 = new District;
            $district81->bezeichnung = 'Lienz';


            $district82 = new District;
            $district82->bezeichnung = 'Reutte';


            $district83 = new District;
            $district83->bezeichnung = 'Schwaz';


            $state9->districts()->saveMany([$district75, $district76,
                $district77, $district78, $district79, $district80, $district81, $district82,
                $district83]);
            $state9->save();

        $state10 = new State;
        $state10->bezeichnung = 'Vorarlberg';
        $state10->save();

            $district84 = new District;
            $district84->bezeichnung = 'Bludenz';


            $district85 = new District;
            $district85->bezeichnung = 'Bregenz';


            $district86 = new District;
            $district86->bezeichnung = 'Dornbirn';


            $district87 = new District;
            $district87->bezeichnung = 'Feldkirch';


            $state10->districts()->saveMany([$district85, $district86,
                $district87, $district84]);
            $state10->save();

        $state6 = new State;
        $state6->bezeichnung = 'Wien';
        $state6->save();

            $district88 = new District;
            $district88->bezeichnung = '1. Innere Stadt';


            $district89 = new District;
            $district89->bezeichnung = '2. Leopoldstadt';


            $district90 = new District;
            $district90->bezeichnung = '3. Landstraße';


            $district91 = new District;
            $district91->bezeichnung = '4. Wieden';


            $district92 = new District;
            $district92->bezeichnung = '5. Margarethen';


            $district93 = new District;
            $district93->bezeichnung = '6. Mariahilf';


            $district94 = new District;
            $district94->bezeichnung = '7. Neubau';


            $district95 = new District;
            $district95->bezeichnung = '8. Josefstad';


            $district96 = new District;
            $district96->bezeichnung = '9. Alsergrund';


            $district97 = new District;
            $district97->bezeichnung = '10. Favoriten';


            $district98 = new District;
            $district98->bezeichnung = '11. Simmering';


            $district99 = new District;
            $district99->bezeichnung = '12. Meidling';


            $district100 = new District;
            $district100->bezeichnung = '13. Hietzing';


            $district101 = new District;
            $district101->bezeichnung = '14. Penzing';

            $district102 = new District;
            $district102->bezeichnung = '15. Rudolfsheim-Fünfhaus';


            $district103 = new District;
            $district103->bezeichnung = '16. Ottakring';


            $district104 = new District;
            $district104->bezeichnung = '17. Hernals';


            $district105 = new District;
            $district105->bezeichnung = '18. Währing';


            $district106 = new District;
            $district106->bezeichnung = '19. Döbling';


            $district107 = new District;
            $district107->bezeichnung = '20. Brigittenau';


            $district108 = new District;
            $district108->bezeichnung = '21. Floridsdorf';


            $district109 = new District;
            $district109->bezeichnung = '22. Donaustadt';


            $district110 = new District;
            $district110->bezeichnung = '23. Liesing';


            $state6->districts()->saveMany([$district88, $district89,
                $district90, $district91, $district92, $district93, $district94, $district95,
                $district96, $district97, $district98, $district99, $district100, $district101, $district102,
                $district103, $district104, $district105, $district106, $district107, $district108, $district109,
                $district110]);
            $state6->save();

    }
}
