<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Vaccinated;
use App\Models\Vacevent;
use DateTime;
use Illuminate\Database\Seeder;

class VaccinatedsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $vaccinated = new Vaccinated;
        $vaccinated->firstname = 'Antonia';
        $vaccinated->lastname = 'Mair';
        $vaccinated->sex = 'w';
        $vaccinated->birthdate = new DateTime('1999-05-08');
        $vaccinated->svnr = '3775';
        $vaccinated->email = 'antonia.mair@outlook.com';
        $vaccinated->tel = '+43 676 717 15 66';
        $vacevent = Vacevent::all()->first();
        $vaccinated->vacevent()->associate($vacevent);
        $user = User::where('id', 2)->first();
        $vaccinated->user()->associate($user);
        $vaccinated->save();
    }
}
