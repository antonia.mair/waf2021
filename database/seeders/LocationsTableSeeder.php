<?php

namespace Database\Seeders;

use App\Models\District;
use App\Models\Location;
use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $location = new Location;
        $location->bezeichnung = 'Design Center Linz';
        $location->street = 'Europaplatz 1';
        $location->plz = '4020';
        $location->ort = 'Linz';
        $location->capacity = 5;
        $district = District::all()->get('id', 1);
        $location->district()->associate($district);
        $location->save();

        $location2 = new Location;
        $location2->bezeichnung = 'BH Freistadt';
        $location2->street = 'Promenade 5';
        $location2->plz = '4240';
        $location2->ort = 'Freistadt';
        $district2 = District::all()->get('id', 2);
        $location2->district()->associate($district2);
        $location2->save();
    }
}
